#include "GLogHolder.hpp"

using namespace google;

namespace WolfBlades
{
	void Writer(const char* data, size_t size)
	{
		LOG(ERROR) << data;
	}

	GLogHolder* GLogHolder::INSTANCE = nullptr;

	GLogHolder::GLogHolder(const char* argv0)
	{
		if (INSTANCE != nullptr)
		{
			return;
		}

		INSTANCE = this;
		FLAGS_log_dir = "./log";
		SetLogFilenameExtension("log");
		FLAGS_alsologtostderr = true;
		InstallFailureSignalHandler();
		InstallFailureWriter(&Writer);
		FLAGS_stop_logging_if_full_disk = true;
		FLAGS_logtostderr = false;
		FLAGS_colorlogtostderr = true;

		InitGoogleLogging(argv0);
	}

	GLogHolder::~GLogHolder()
	{
		ShutdownGoogleLogging();
		INSTANCE = nullptr;
	}
}
