#include "ProgramConfigurator.hpp"

namespace WolfBlades
{
    /// @brief 内部的JsonRoot，储存从文件中读取的json信息
    Json::Value json_root;
    const Json::Value &GetJsonRoot()
    {
        return json_root;
    }

    /// @brief 内部的是否解析完成的标志
    bool parsing_finished;
    bool GetIsParsingFinished()
    {
        return parsing_finished;
    }

    void InitializeConfiguration(const std::string &filePath)
    {
        json_root = Json::Value();
        parsing_finished = false;
        Json::Reader reader;
        std::ifstream file(filePath);

        if (!file.is_open())
        {
            LOG(ERROR) << "Error when open json file: " << filePath << "\n";
            return;
        }

        LOG(INFO) << "Found json file, ready to read:" << filePath << "\n";
        parsing_finished = reader.parse(file, json_root);
        if (parsing_finished)
        {
            LOG(INFO) << "Finish parsing json, ready to configure parameters\n";
        }
        else
        {
            LOG(ERROR) << "Error when parsing json file: " << filePath << "\n";
        }
        file.close();
        parsing_finished = true;
    }

    void ShutdownConfiguration()
    {
        json_root.clear();
        parsing_finished = false;
    }

}
