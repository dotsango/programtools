#pragma once

#include <fstream>                    // for file reading
#include <vector>                     // vector type to store string json keys
#include <string>                     // string type for json keys
#ifdef _WIN32
#include <json/json.h>
#else
#include <jsoncpp/json/json.h>        // json file support
#endif
#include <boost/algorithm/string.hpp> // methods for split string keys
#include <exception>                  // for error process
#include "GLogHolder.hpp"             // for log output

namespace WolfBlades
{
	/**
	 * \brief 获取Json文件数据
	 * \return 读取到的Json文件的数据
	 */
	const Json::Value &GetJsonRoot();

	/**
	 * \brief 获取是否完成了解析Json数据的步骤
	 * \return 是否完成了解析Json数据的步骤
	 */
	bool GetIsParsingFinished();

	/**
	 * \brief 初始化配置信息
	 * \param filePath 配置文件的路径
	 */
	void InitializeConfiguration(const std::string &filePath);

	/**
	 * \brief 配置参数
	 * \tparam T 参数类型
	 * \param key 参数的key，点号表示层级，例如 a.b 表示目标 value 是 {"a":{"b":value}}
	 * \param value 传入参数本身
	 * \return 是否配置成功
	 */
	template <typename T>
	bool Configure(const std::string &key, T &value)
	{
		if (!GetIsParsingFinished())
		{
			LOG(ERROR) << "Parsing is not finished, cannot configure parameters";
			return false;
		}

		bool result = true;
		Json::Value root = GetJsonRoot();
		try
		{
			std::vector<std::string> keys;
			split(keys, key, boost::is_any_of("."), boost::token_compress_on);
			for (int i = 0; i < keys.size(); i++)
			{
				root = root[keys[i]];
				if (root.empty())
				{
					throw "Empty json value";
				}
			}
			value = root.as<T>();
		}
		catch (const char *ex)
		{
			result = false;
			LOG(ERROR) << "Error when parsing key(" << key << "), please check the possible problems:\n1. the format of target key in program\n2. the format of target key in json file\n3. the type of the key in json file matches the key in program\n"
								   << ex;
		}
		catch (std::exception ex)
		{
			result = false;
			LOG(ERROR) << "Error when parsing key(" << key << "), please check the possible problems:\n"
								   << ex.what();
		}

		return result;
	}

	/**
	 * \brief 关闭配置，释放资源
	 */
	void ShutdownConfiguration();
}
