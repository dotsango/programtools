#pragma once

#include <glog/logging.h>

namespace WolfBlades
{
	/**
	 * \brief 代表Glog
	 */
	class GLogHolder
	{
	protected:
		static GLogHolder* INSTANCE;

	public:
		/**
		 * \brief 构造并初始化一个glog（注意，此类并不会重复初始化）
		 * \param argv0 传入程序的argv[0]
		 */
		GLogHolder(const char* argv0);

		/**
		 * \brief 销毁并关闭日志
		 */
		~GLogHolder();
	};
}